import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {BooklistProvider} from '../../providers/booklist-provider/booklist-provider';
import {FilterProvider} from '../../providers/filter-provider/filter-provider';
import {BookDetailPage} from '../book-detail/book-detail';

@Component({
templateUrl: 'build/pages/home/home.html',
providers: [BooklistProvider,FilterProvider],
styles: [`
.item-select{
width: 100%;
}
`],
})
export class HomePage {
public bookList=[];
public filterObject:any;
public filterData:any;
public sortData:any;
token:any;
constructor(public navCtrl: NavController,private bookListProvider:BooklistProvider,private filterProvider:FilterProvider)
{



this.token=localStorage.getItem("token");
this.filterProvider.getFilters(this.token).subscribe(filterdata=>{

this.filterData=filterdata.filters;
this.sortData=filterdata.sort;
console.log('filters :'+JSON.stringify(filterdata));
this.getbooks();

},error =>{

console.log("error man");
});


} // Constructor


getbooks()
{
this.bookListProvider.getBookList(this.token).subscribe(data=>{
let  array = [];
array= data.result;
for (let item=0;item<array.length;item++) {
array[item]["mobileimage"]=array[item]["image-2"];
this.bookList.push(array[item]);
}
console.log('data :'+JSON.stringify(this.bookList));
},error =>{

console.log("error man");
});

}


loadMore(infiniteScroll)
{
infiniteScroll.complete();
this.bookListProvider.getBookList(this.token).subscribe(data=>{

let  array = [];
array= data.result;
for (let item=0;item<array.length;item++) {
array[item]["mobileimage"]=array[item]["image-2"];
this.bookList.push(array[item]);
//console.log(array[item]["image-2"]);
}

},error =>{
infiniteScroll.complete();
console.log("error man");
});


}


onItemClick(book)
{

this.navCtrl.push(BookDetailPage,book);
}

}
