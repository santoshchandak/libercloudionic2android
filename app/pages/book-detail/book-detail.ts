import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

/*
Generated class for the BookDetailPage page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/
@Component({
templateUrl: 'build/pages/book-detail/book-detail.html',
})
export class BookDetailPage {

bookDetail:any;
constructor(private navCtrl: NavController,private navParams: NavParams) {

this.bookDetail=navParams;

console.log(JSON.stringify(this.bookDetail));
}

}
