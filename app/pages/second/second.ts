import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';

@Component({
  templateUrl: 'build/pages/second/second.html'
})
export class HomePage {
  constructor(public navCtrl: NavController) {

  }
}
